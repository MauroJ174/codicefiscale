package it.dstech.DAO;

import it.dstech.model.User;

public interface UserDao {
	
	public boolean create(User user);

	public boolean update(User user);

	public boolean delete(Long id);

//	public User searchByCF(String CF);

	public String searchByUser(User user);
}
