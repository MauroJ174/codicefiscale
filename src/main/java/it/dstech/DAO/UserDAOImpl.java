package it.dstech.DAO;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import it.dstech.model.User;
import it.dstech.service.UserService;

@Repository
public class UserDAOImpl implements UserDao {
	
	@Autowired
	public SessionFactory sessionFactory;
	
	
	@Override
	public boolean create(User user) {
		sessionFactory.getCurrentSession().persist(user);
		sessionFactory.getCurrentSession().flush();
		return true;
	}

	@Override
	public boolean update(User user) {
		sessionFactory.getCurrentSession().merge(user);
		sessionFactory.getCurrentSession().flush();
		return true;
	}

	@Override
	public boolean delete(Long id) {
		User find = sessionFactory.getCurrentSession().find(User.class, id);
		sessionFactory.getCurrentSession().remove(find);
		sessionFactory.getCurrentSession().flush();
		return true;
	}

	@Override
	public String searchByUser(User user) {
		String hql = "FROM user u WHERE u.nome = "+ user.getNome() +"AND u.cognome = "+user.getCognome()+"AND u.sesso = "+user.isSesso()+
		"AND u.comune = "+user.getComune()+"AND u.datanascita = "+user.getDataNascita();
		User u = (User) sessionFactory.getCurrentSession().createQuery(hql).getSingleResult();
		if(u != null)
		return u.getCodiceFiscale();
		else return null;
	}

}
