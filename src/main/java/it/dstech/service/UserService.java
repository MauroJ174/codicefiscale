package it.dstech.service;

import it.dstech.model.User;

public interface UserService {

	public boolean create(User user);

	public boolean update(User user);

	public boolean delete(Long id);

	public String searchByUser(User user);
	
	public String creaCF(User user);
}
