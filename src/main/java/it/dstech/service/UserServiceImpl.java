package it.dstech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.dstech.DAO.UserDao;
import it.dstech.model.User;
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDAO;

	@Override
	public boolean create(User user) {
		user.setCal(user.getG(), user.getM(), user.getA());
		return userDAO.create(user);
	}

	@Override
	public boolean update(User user) {
		return userDAO.update(user);
	}

	@Override
	public boolean delete(Long id) {
		return userDAO.delete(id);
	}

	@Override
	public String searchByUser(User user) {
		user.setCal(user.getG(), user.getM(), user.getA());
		return userDAO.searchByUser(user);
	}

	@Override
	public String creaCF(User user) {
	return null;	
	}

}
