package it.dstech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.dstech.model.User;
import it.dstech.service.UserService;
@RestController
public class UserController {
	
	@Autowired
	UserService user;

	@RequestMapping(value = "/model", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User model() {
		User modello = new User();
		return modello;
	}

	@RequestMapping(value = "/user", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean updateUser(@RequestBody User u) {
		return user.update(u);
	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean rimuoviAuto(@PathVariable(value = "id") Long id) {
		return user.delete(id);
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String ricercaCF(@RequestBody User u) {
		String cf = user.searchByUser(u);
		if (cf != null)
			return cf ;
		else {
			String codice = user.creaCF(u);
			user.create(u);
		return codice;
		}
	}
	
}
