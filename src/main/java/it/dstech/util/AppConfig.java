package it.dstech.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "it.dstech")// excludeFilters = {
//		@Filter(type = FilterType.ANNOTATION, value = Configuration.class) })
public class AppConfig {

}
